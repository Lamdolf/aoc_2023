-- PART 1
maxRed   = 12
maxGreen = 13
maxBlue  = 14

gameId :: String -> Integer 
gameId xs = read id :: Integer 
            where 
                id = last $ words $ takeWhile (/=':') xs 

part1 :: [String] -> [Integer]
part1 (x:xs) = if False `elem` body then
                  part1 xs
               else
                  id : part1 xs
               where
                   id   = gameId x
                   body = map evaluateGame $ parseGame x
part1 [] = []

evaluateGame :: (String, Integer) -> Bool
evaluateGame game = case fst game of 
                      "green" -> snd game <= maxGreen
                      "blue"  -> snd game <= maxBlue
                      "red"   -> snd game <= maxRed
                      _       -> False -- why?

-- PART 2
-- For part 2, find minimum for the game to be valid
findMinimum :: Integer  -> [(String, Integer)] -> Integer
findMinimum min (x:xs) = if snd x > min then
                            findMinimum (snd x) xs 
                         else
                            findMinimum min xs 
findMinimum min [] = min

getPower :: [(String, Integer)] -> Integer
getPower all@(x:xs) = green * blue * red
                      where
                          green = findMinimum 0 $ filter (\(x,y) -> x == "green") all
                          blue  = findMinimum 0 $ filter (\(x,y) -> x == "blue") all
                          red   = findMinimum 0 $ filter (\(x,y) -> x == "red") all
getPower [] = 1


-- Shared between the two parts
removeCommas :: String -> String
removeCommas (x:xs) = if x `notElem` ";," 
                        then x : removeCommas xs
                      else
                        removeCommas xs 
removeCommas [] = ""

gameToTuples :: [String] -> [(String, Integer)]
gameToTuples (x:y:xs) = (color, id) : gameToTuples xs
                        where
                            color = y
                            id = read x :: Integer
gameToTuples [] = []
gameToTuples [[]] = []
gameToTuples [_:_] = []
                    

parseGame :: String -> [(String, Integer)]
parseGame gameLine = gameToTuples game
                     where
                         game = words 
                               .removeCommas 
                               .drop 2 
                               $dropWhile (/=':') gameLine

main :: IO ()
main = do
    inData <- readFile "./in1"
    putStrLn "First task"
    print . sum . part1 $ lines inData -- 2283
    putStrLn "Second task"
    print . sum . map (getPower . parseGame) $ lines inData -- 78669
