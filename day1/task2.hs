import Data.Char

-- From u/Pristine_Western600 on r/Haskell (i'm still learning)
getDigits :: String -> String
getDigits [] = []
getDigits xs = 
    let rem = tail xs in
        case xs of
        ('o':'n':'e':_)          -> '1' : getDigits rem
        ('t':'w':'o':_)          -> '2' : getDigits rem
        ('t':'h':'r':'e':'e':_)  -> '3' : getDigits rem
        ('f':'o':'u':'r':_)      -> '4' : getDigits rem
        ('f':'i':'v':'e':_)      -> '5' : getDigits rem
        ('s':'i':'x':_)          -> '6' : getDigits rem
        ('s':'e':'v':'e':'n':_)  -> '7' : getDigits rem
        ('e':'i':'g':'h':'t':_)  -> '8' : getDigits rem
        ('n':'i':'n':'e':_)      -> '9' : getDigits rem
        (x:_) ->
          if isDigit x
            then x : getDigits rem
            else getDigits rem

toValues :: String -> Integer
toValues xs = read $ head xs : [last xs] :: Integer 

main :: IO ()
main = do
    inValues <- readFile "./in1"
    print . sum . map (toValues . getDigits) $ lines inValues
    
