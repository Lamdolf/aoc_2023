import Data.Char

-- Take only digits
getDigits :: String -> String
getDigits (x:xs) 
  | isDigit x = x : getDigits xs
  | not $ isDigit x = getDigits xs
  | otherwise = ""
getDigits [] = ""

-- Take first and last, and return an integer
stoi :: String -> Integer
stoi xs = read (head xs : [last xs]) :: Integer

-- processLines :: String -> String
-- processLines xs =

main :: IO()
main = do
    inValues <- readFile "./in1"
    print $  sum $ map (stoi . getDigits) (lines inValues)
